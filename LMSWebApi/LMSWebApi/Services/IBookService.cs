﻿using LMSWebApi.DbEntity;
using LMSWebApi.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMSWebApi.Services
{
    public interface IBookService
    {
        Task<Book> GetBookAsync(int id);
        Task<IEnumerable<Book>> GetAllBooksAsync();
        Task SaveBookAsync(BookInputDto book);
        Task UpdateBookAsync(BookInputDto bookInputDto);
        Task DeleteBookAsync(int id);
       
    }
}
