﻿using LMSWebApi.DbEntity;
using LMSWebApi.Dtos;
using LMSWebApi.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMSWebApi.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;

        public BookService(IBookRepository bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public async Task<Book> GetBookAsync(int id)
        {
            return await _bookRepository.GetAsync(id);
        }

        public async Task<IEnumerable<Book>> GetAllBooksAsync()
        {
            return await _bookRepository.GetAllAsync();
        }

        public async Task SaveBookAsync(BookInputDto bookInputDto)
        {
            var book = new Book
            {
                Id = bookInputDto.Id,
                Name = bookInputDto.Name,
                Publisher = bookInputDto.Publisher,
                ISBN = bookInputDto.ISBN,
                Author = bookInputDto.Author,
                Edition = bookInputDto.Edition
            };
            await _bookRepository.SaveAsync(book);
        }

        public async Task UpdateBookAsync(BookInputDto bookInputDto)
        {
            var book = new Book
            {
                Id = bookInputDto.Id,
                Name = bookInputDto.Name,
                Publisher = bookInputDto.Publisher,
                ISBN = bookInputDto.ISBN,
                Author = bookInputDto.Author,
                Edition = bookInputDto.Edition
            };
            await _bookRepository.UpdateAsync(book);
        }

        public async Task DeleteBookAsync(int id)
        {
             await _bookRepository.DeleteAsync(id);
        }
    }
}
