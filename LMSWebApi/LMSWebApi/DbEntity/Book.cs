﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LMSWebApi.DbEntity
{
    [Table("Book")]
    public class Book
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [Required]
        [MaxLength(256)]
        public string Publisher { get; set; }

        [MaxLength(50)]
        public string ISBN { get; set; }

        [Required]
        [MaxLength(256)]
        public string Author { get; set; }

        [MaxLength(50)]
        public string Edition { get; set; }
    }
}
