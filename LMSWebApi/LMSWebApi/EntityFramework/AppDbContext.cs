﻿using LMSWebApi.DbEntity;
using Microsoft.EntityFrameworkCore;

namespace LMSWebApi.EntityFramework
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public DbSet<Book> Book { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Book>().HasData(
                new Book
                {
                    Id = 1,
                    Name = "Electronics",
                    Publisher = "Taleju Publications",
                    ISBN = "142252-SFDFDE-45123",
                    Author = "Ram Sharan Karki",
                    Edition = "First"
                },
                new Book
                {
                    Id = 2,
                    Name = "MathMatics",
                    Publisher = "Buddha Publications",
                    ISBN = "142252-SFDFDE-45123",
                    Author = "Rupak Kumar",
                    Edition = "Second"
                }
            );
        }
    }
}
