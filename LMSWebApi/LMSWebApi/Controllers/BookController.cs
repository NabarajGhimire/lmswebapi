﻿using System;
using System.Collections.Generic;
using LMSWebApi.Dtos;
using LMSWebApi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using LMSWebApi.DbEntity;

namespace LMSWebApi.Controllers
{
    [Route("lms/v1/")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly ILogger<BookController> _logger;
        private readonly IBookService _bookService;
        public BookController(ILogger<BookController> logger, IBookService bookService)
        {
            _logger = logger;
            _bookService = bookService;
        }

        [Route("books")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookOutputDto>>> GetAll()
        {
            var bookList = new List<BookOutputDto>();
            try
            {
                var books = await _bookService.GetAllBooksAsync();
                foreach (var book in books)
                {
                    var model = new BookOutputDto()
                    {
                        Id = book.Id,
                        Name = book.Name,
                        Publisher = book.Publisher,
                        ISBN = book.ISBN,
                        Author = book.Author,
                        Edition = book.Edition
                    };
                    bookList.Add(model);
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
            }
            return Ok(bookList);
        }

        [Route("books/{id}")]
        [HttpGet]
        public async Task<ActionResult<BookOutputDto>> Get(int id)
        {
            BookOutputDto bookModel = new BookOutputDto();
            try
            {
                var book = await _bookService.GetBookAsync(id);
                if (book != null)
                {
                    bookModel = new BookOutputDto()
                    {
                        Id = book.Id,
                        Name = book.Name,
                        Publisher = book.Publisher,
                        ISBN = book.ISBN,
                        Author = book.Author,
                        Edition = book.Edition
                    };
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
            }
            return Ok(bookModel);
        }

        [Route("books")]
        [HttpPost]
        public async Task<ActionResult<string>> Post(BookInputDto book)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _bookService.SaveBookAsync(book);
                    return Ok("Success");
                }
                return Ok("Invalid");
            }
            catch (Exception Ex)
            {
                _logger.LogInformation(Ex.Message);
                return Ok("Error");
            }
        }

        [Route("books/{id}")]
        [HttpPut]
        public async Task<ActionResult<string>> Put(int id, BookInputDto bookInputDto)
        {
            try
            {
                if (id == bookInputDto.Id)
                {
                    if (ModelState.IsValid)
                    {
                        await _bookService.UpdateBookAsync(bookInputDto);
                        return Ok("Success");
                    }
                }
                return Ok("Invalide");
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
                return Ok("Error");
            }
        }

        [Route("books/{id}")]
        [HttpDelete]
        public async Task<ActionResult<string>> Delete(int id)
        {
            try
            {
                var book = await _bookService.GetBookAsync(id);
                if (book == null)
                {
                    return NotFound();
                }
                await _bookService.DeleteBookAsync(id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
                return Ok("Error");
            }
        }

    }
}
