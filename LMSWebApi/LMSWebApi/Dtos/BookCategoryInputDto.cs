﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMSWebApi.Dtos
{
    public class BookCategoryInputDto
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryAlias { get; set; }
    }
}
