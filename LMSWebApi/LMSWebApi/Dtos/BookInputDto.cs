﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LMSWebApi.Dtos
{
    public class BookInputDto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; }

        [Required]
        [MaxLength(256)]
        public string Publisher { get; set; }

        [MaxLength(50)]
        public string ISBN { get; set; }

        [Required]
        [MaxLength(256)]
        public string Author { get; set; }

        [MaxLength(50)]
        public string Edition { get; set; }
    }
}
