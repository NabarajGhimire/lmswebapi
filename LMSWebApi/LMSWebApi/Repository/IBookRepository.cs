﻿using LMSWebApi.DbEntity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMSWebApi.Repository
{
    public interface IBookRepository
    {
        Task<Book> GetAsync(int id);

        Task<IEnumerable<Book>> GetAllAsync();

        Task SaveAsync(Book book);

        Task UpdateAsync(Book book);

        Task DeleteAsync(int id);
    }
}
