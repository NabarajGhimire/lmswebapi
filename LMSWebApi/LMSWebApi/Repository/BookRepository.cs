﻿using LMSWebApi.DbEntity;
using LMSWebApi.EntityFramework;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMSWebApi.Repository
{
    public class BookRepository : IBookRepository
    {
        private AppDbContext _context;
        private DbSet<Book> _entity;

        public BookRepository(AppDbContext context)
        {
            _context = context;
            _entity = context.Set<Book>();
        }

        public async Task<Book> GetAsync(int id)
        {
            return await _entity.FindAsync(id).ConfigureAwait(false);
        }

        public async Task<IEnumerable<Book>> GetAllAsync()
        {
            return await _entity.ToListAsync().ConfigureAwait(false);
        }

        public async Task SaveAsync(Book book)
        {
            _context.Entry(book).State = EntityState.Added;
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task UpdateAsync(Book book)
        {
            _context.Entry(book).State = EntityState.Modified;
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAsync(int id)
        {
            var bookData = await GetAsync(id).ConfigureAwait(false);
            _context.Entry(bookData).State = EntityState.Deleted;
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

    }
}
