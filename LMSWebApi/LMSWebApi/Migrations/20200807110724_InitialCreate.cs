﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LMSWebApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Book",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Publisher = table.Column<string>(maxLength: 256, nullable: false),
                    ISBN = table.Column<string>(maxLength: 50, nullable: true),
                    Author = table.Column<string>(maxLength: 256, nullable: false),
                    Edition = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Book", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "Id", "Author", "Edition", "ISBN", "Name", "Publisher" },
                values: new object[] { 1, "Ram Sharan Karki", "First", "142252-SFDFDE-45123", "Electronics", "Taleju Publications" });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "Id", "Author", "Edition", "ISBN", "Name", "Publisher" },
                values: new object[] { 2, "Rupak Kumar", "Second", "142252-SFDFDE-45123", "MathMatics", "Buddha Publications" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Book");
        }
    }
}
